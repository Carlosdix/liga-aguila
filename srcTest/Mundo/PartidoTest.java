package Mundo;

import junit.framework.TestCase;

public class PartidoTest extends TestCase
{
    private Partido primero;

    private Partido ultimo;

    private void crearEscenario1()
    {
        primero = new Partido( "Dep. Pasto", "Ind. Medellín", 1, 0,
                2, 3, 0 ,0 );
        ultimo = primero;
    }

    private void crearEscenario2()
    {
        crearEscenario1();
        ultimo = ultimo.agregarPartido( "Ind. Medellín", "Dep. Pasto", 3, 2,
                2, 3, 1 ,0 );
        ultimo = ultimo.agregarPartido( "Dep. Pasto", "Dep. Cali", 2, 0,
                2, 3, 0 ,0 );
        ultimo = ultimo.agregarPartido( "Dep. Cali", "Dep. Pasto", 2, 2,
                2, 3, 0 ,0 );
    }

    public void testCantidadPartidos()
    {
        crearEscenario1();
        assertEquals( 1, primero.contarPartidos( 1 ) );
        crearEscenario2();
        assertEquals( 4, primero.contarPartidos( 1 ) );
    }

    public void testInsercion()
    {
        crearEscenario2();
        assertNotNull( primero );
        assertNotNull( primero.darSiguiente() );
        assertNotNull( primero.darSiguiente().darSiguiente() );
        assertNotNull( primero.darSiguiente().darSiguiente().darSiguiente() );
    }

    public void testResultado()
    {
        crearEscenario1();
        assertEquals( "Se esperaba que 'gano' para el partido",Partido.RESULTADOS[ 0 ], primero.darResultado( "Dep. Pasto" ) );
    }
}
