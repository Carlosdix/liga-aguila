package Mundo;

import junit.framework.TestCase;

import javax.swing.*;
import java.io.File;

public class EquipoTest extends TestCase
{
    private Equipo equipo;

    private void crearEscenario1()
    {
        ImageIcon imageIcon = null;
        try
        {
            imageIcon = new ImageIcon( "./imagenes/logos/Deportivo_Pasto_logo.png" );
        }
        catch ( Exception e )
        {
            fail( "El logo del equipo no existe" );
        }
        equipo = new Equipo( "Dep. Pasto", imageIcon );
    }

    public void crearEscenario2()
    {
        ImageIcon imageIcon = null;
        try
        {
            imageIcon = new ImageIcon( "./imagenes/logos/Deportivo_Pasto_logo.png" );
        }
        catch ( Exception e )
        {
            fail( "El logo del equipo no existe" );
        }
        equipo = new Equipo( "Ind. Medellín", imageIcon );
        try
        {
            equipo.agregarEquipo( "Envigado", new ImageIcon( "./imagenes/logos/Envigado FC.png" ) );
            equipo.agregarEquipo( "Ind. Santa Fe", new ImageIcon( "./imagenes/logos/SFE.png" ) );
            equipo.agregarEquipo( "Jaguares de Córdoba", new ImageIcon( "./imagenes/logos/1200px-JAGUARES_DE_CORDOBA.svg.png" ) );
            equipo.agregarEquipo( "Junior", new ImageIcon( "./imagenes/logos/1200px-Escudo_de_Atlético_Junior.svg.png" ) );
            equipo.agregarEquipo( "Dep. Pasto", new ImageIcon( "./imagenes/logos/Deportivo_Pasto_logo.png" ) );
            equipo.agregarEquipo( "La Equidad", new ImageIcon( "./imagenes/logos/Equidad Fc.png" ) );
        }
        catch (Exception e)
        {
            fail( "No debio ocurrir esto, se deberian haber agregado los equipos" );
        }
    }

    public void testInsercionEquipos()
    {
        crearEscenario1();
        try
        {
            ImageIcon imageIcon = null;
            equipo.agregarEquipo( "Dep. Pasto", null );
            fail( "Esto no debio que haber ocurrido, ya se encontraba registrado el equipo 'Dep. Pasto'" );
        }
        catch ( Exception e )
        {
            assertTrue( "El equipo Dep. Pasto ya se encontraba registrado",true );
        }
        crearEscenario2();
        try 
        {
            equipo.agregarEquipo( "Tolima", null );
            equipo.agregarEquipo( "Pereira", null );
            equipo.agregarEquipo( "Rionegro Aguilas Doradas", null );
            equipo.agregarEquipo( "Junior", null );
            fail( "El equipo 'Junior' no se debio agregar" );
        }
        catch ( Exception e )
        {
            assertTrue( "El equipo 'Junior' ya se encontranba registrado",true );
        }
    }

    public void testBuscarEquipo()
    {
        crearEscenario2();
        assertNotNull( equipo.buscarEquipo( "Dep. Pasto" ) );
        assertNotNull( equipo.buscarEquipo( "Junior" ) );
        assertNotNull( equipo.buscarEquipo( "Envigado" ) );
        assertNotNull( equipo.buscarEquipo( "Jaguares de Córdoba" ) );
        assertNull( equipo.buscarEquipo( "Tolima" ) );
        assertNull( equipo.buscarEquipo( "Dep. Cali" ) );
    }

    public void testAgregarPartido()
    {
        crearEscenario1();
        try
        {
            equipo.agregarEquipo( "Ind. El Charco", null );
        }
        catch (Exception e)
        {
            fail( "No debio ocurrir eso, el equipo no se encontraba registrado, por ende, debio hacer la insercion" );
        }
        equipo.agregarPartido( equipo.darNombre(),"Ind. El Charco", 0, 0, 0 ,0 ,0, 0 );
        equipo.agregarPartido( "Ind. El Charco", equipo.darNombre(), 0, 0, 0 ,0 ,0, 0 );
    }

    public void testBalance()
    {
        try
        {
            crearEscenario1();
            equipo.darBalance();
            fail( "No hay partidos, no deberia haber llegado aqui" );
        }
        catch ( Exception e )
        {
            assertTrue( true );
        }
        try
        {
            equipo.agregarPartido( equipo.darNombre(),"Ind. El Charco", 1, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Ind. El Charco", equipo.darNombre(), 0, 1, 0 ,0 ,0, 0 );
            equipo.agregarPartido( equipo.darNombre(),"Consaca Fc", 0, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Consaca Fc", equipo.darNombre(), 0, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( equipo.darNombre(),"Ipiales", 0, 1, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Ipiales", equipo.darNombre(), 1, 0, 0 ,0 ,0, 0 );
            System.out.println( "testBalance" +"\n"+
                    "{"+"\n"+
                        equipo.darBalance()+"\n"+
                    "}" );
        }
        catch (Exception e)
        {
            fail( "No se completo el balance: "+e.getMessage() );
        }
    }

    public void testResumenEstrategiaParoli()
    {
        crearEscenario1();
        try
        {
            equipo.agregarPartido( equipo.darNombre(),"Ind. El Charco", 1, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Ind. El Charco", equipo.darNombre(), 0, 1, 0 ,0 ,0, 0 );
            equipo.agregarPartido( equipo.darNombre(),"Consaca Fc", 0, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Consaca Fc", equipo.darNombre(), 0, 0, 0 ,0 ,0, 0 );
            equipo.agregarPartido( equipo.darNombre(),"Ipiales", 0, 1, 0 ,0 ,0, 0 );
            equipo.agregarPartido( "Ipiales", equipo.darNombre(), 1, 0, 0 ,0 ,0, 0 );
            System.out.println( equipo.darResumenEstrategiaParoli() );
        }
        catch (Exception e)
        {
            fail( "Error inesperado: "+e.getMessage() );
        }
    }
}
