package Mundo;

public class Partido
{
    public final static String RESULTADOS[] = {"Gano","Perdio","Empato"};

    private final String nombreLocal;

    private final String nombreVisitante;

    private final int golesLocal;

    private final int golesVisitante;

    private final int tarjetasAmarillasLocal;

    private final int tarjetasAmarillasVisitante;

    private final int tarjetasRojasLocal;

    private final int tarjetasRojasVisitante;

    private Partido anterior;

    private Partido siguiente;

    public Partido( final String pNombreLocal, final String pNombreVisitante, final  int pGolesLocal,
                    final int pGolesvisitante, final int pTALocal, final int pTAVisitante, final int pTRLocal,
                    final int pTRVisitante )
    {
        nombreLocal = pNombreLocal;
        nombreVisitante = pNombreVisitante;
        golesLocal = pGolesLocal;
        golesVisitante = pGolesvisitante;
        tarjetasAmarillasLocal = pTALocal;
        tarjetasAmarillasVisitante = pTAVisitante;
        tarjetasRojasLocal = pTRLocal;
        tarjetasRojasVisitante = pTRVisitante;
        anterior = null;
        siguiente = null;
    }

    public Partido darAnterior()
    {
        return anterior;
    }

    public Partido darSiguiente()
    {
        return siguiente;
    }

    public int contarPartidos( int pAcumulado )
    {
        return siguiente != null ?  siguiente.contarPartidos( pAcumulado+1 ) : pAcumulado;
    }

    public Partido agregarPartido( final String pNombreLocal, final String pNombreVisitante, final  int pGolesLocal,
                                final int pGolesvisitante, final int pTALocal, final int pTAVisitante, final int pTRLocal,
                                final int pTRVisitante )
    {
        siguiente = new Partido( pNombreLocal, pNombreVisitante, pGolesLocal, pGolesvisitante, pTALocal ,pTAVisitante, pTRLocal, pTAVisitante );
        siguiente.anterior = this;
        return siguiente;
    }

    public String darResultado( final String pNombre )
    {
        if ( golesLocal == golesVisitante )
        {
            return RESULTADOS[2];
        }
        else if( pNombre.equalsIgnoreCase( nombreLocal ) )
        {
            return golesLocal > golesVisitante ? RESULTADOS[0] :  RESULTADOS[1];
        }
        else
        {
            return golesLocal > golesVisitante ? RESULTADOS[1] : RESULTADOS[0];
        }
    }

    public String buscarEncuentro( final String pEquipo1, final String pEquipo2 )
    {
        if( (nombreLocal.equals( pEquipo1 ) && nombreVisitante.equals( pEquipo2 ) ) || ( nombreLocal.equals( pEquipo2 ) && nombreVisitante.equals( pEquipo1 ) ) )
        {
            return nombreLocal+" ("+golesLocal+"-"+golesVisitante+") "+nombreVisitante;
        }
        return siguiente == null ? null : siguiente.buscarEncuentro( pEquipo1, pEquipo2 );
    }

    public int calcularPuntos( final String pnombreEquipo )
    {
        int puntos = darResultado( pnombreEquipo ).equals( RESULTADOS[ 0 ] ) ? 3
                : darResultado( pnombreEquipo ).equals( RESULTADOS[ 1 ] ) ? 0 : 1;
        return siguiente == null ?  puntos : siguiente.calcularPuntos( pnombreEquipo )+puntos ;
    }

    public String toString()
    {
        return "Partido\n" +
                "{" +
                    "nombreLocal='" + nombreLocal + '\'' +
                    ", nombreVisitante='" + nombreVisitante + '\'' +
                '}';
    }
}
