package Mundo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.util.ArrayList;

public class Equipo
{
	private final String nombre;

	private final ImageIcon logo;

	private Equipo izquierda;

	private Equipo derecha;

	private Partido primero;

	private Partido ultimo;

	public Equipo(final String pNombre, final ImageIcon pImageIcon )
	{
		nombre = pNombre;
		logo = pImageIcon;
		izquierda = null;
		derecha = null;
		primero = null;
		ultimo = null;
	}

	public String darNombre()
	{
		return nombre;
	}

	public ImageIcon darLogo()
	{
		return logo;
	}

	public Equipo agregarEquipo(final String pNombre, final ImageIcon pImageIcon ) throws Exception
	{
		final int comparacion = nombre.compareToIgnoreCase( pNombre );
		if ( comparacion == 0 )
		{
			throw new Exception("El Equipo ya se encuentra registrado!");
		}
		else if ( comparacion > 0 )
		{
			return izquierda == null ? izquierda = new Equipo(pNombre, pImageIcon) : izquierda.agregarEquipo(pNombre, pImageIcon);
		}
		else
		{
			return derecha == null ? derecha = new Equipo(pNombre, pImageIcon) : derecha.agregarEquipo(pNombre, pImageIcon);
		}
	}

	public Equipo buscarEquipo( final String pNombre )
	{
		final int comparacion = nombre.compareToIgnoreCase( pNombre );
		if( comparacion == 0 )
		{
			return this;
		}
		else if( comparacion > 0 )
		{
			return izquierda == null ? null : izquierda.buscarEquipo( pNombre );
		}
		else
		{
			return derecha == null ? null : derecha.buscarEquipo( pNombre );
		}
	}

	public void agregarPartido( final String pNombreLocal, final String pNombreVisitante, final  int pGolesLocal,
								final int pGolesvisitante, final int pTALocal, final int pTAVisitante, final int pTRLocal,
								final int pTRVisitante )
	{
		if( ultimo != null )
		{
			ultimo = ultimo.agregarPartido( pNombreLocal,pNombreVisitante,pGolesLocal,pGolesvisitante,pTALocal,pTAVisitante,pTRLocal,pTRVisitante );
		}
		else
		{
			primero = new Partido( pNombreLocal,pNombreVisitante,pGolesLocal,pGolesvisitante,pTALocal,pTAVisitante,pTRLocal,pTRVisitante );
			ultimo = primero;
		}
	}

	public int contarPartidos()
	{
		return primero == null ? 0 : primero.contarPartidos( 1 );
	}

	private int datosBalance()[] throws Exception
	{

		if( primero == null )
		{
			throw new Exception( "No hay partidos registrados" );
		}
		int ganados = 0;
		int perdidos = 0;
		int empatados = 0;
		for( Partido p = primero ; p != null ; p = p.darSiguiente() )
		{
			if( p.darResultado( nombre ).equals( Partido.RESULTADOS[ 0 ] ) )
			{
				ganados++;
			}
			else if( p.darResultado( nombre ).equals( Partido.RESULTADOS[ 1 ] ) )
			{
				perdidos++;
			}
			else
			{
				empatados++;
			}
		}
		return new int[] {ganados,perdidos,empatados,(ganados+empatados+perdidos)};
	}

	public String darBalance()throws Exception
	{
		if( primero == null )
		{
			throw new Exception( "No hay partidos registrados" );
		}
		final int datos[] = datosBalance();
		int ganados = datos[0];
		int perdidos = datos[1];
		int empatados =datos[2];
		int total = datos[3];
		return "Partidos ganados: "+ganados+"\n"+"Partidos perdidos: "+perdidos+"\n"+"Partidos empatados: "+empatados+
				"\n"+"Total: "+total+"\n"+"Acierto: "+((double)Math.round(10000*ganados/total)/100)+"%"+"\n"+"Derrotas: "+((double)Math.round(10000*(double)perdidos/total)/100)+"%"+
				"\n"+"Emparejamiento: "+((double)Math.round(10000*(double)empatados/total)/100)+"%";
	}

	/**
	 * Este metodo es para apuestas de cuota baja pero alta probabilidad
	 * @return El resumen del metodo Paroli
	 * @throws Exception
	 */
	public String darResumenEstrategiaParoli() throws Exception
	{
		final int totalPartidos = contarPartidos();
		if(  totalPartidos >= 2 )
		{
			final int datos[] = datosBalance();
			int ganados = datos[0];
			int perdidos = datos[1];
			int empatados =datos[2];
			int total = datos[3];

			final double pGanados = (double)ganados/total ;
			final double pPerdidos = (double)perdidos/total ;
			final double pEmpatados = (double)empatados/total ;

			String msg = darBalance()+"\n"+"r = Ganados \t r = Perdidos \t r = Empatados \t r = GanaEmpata \t "+
					"r = PierdeEmpata \t r = GanaPierde\n";

			for ( int i = 1 ; i < 4 ; i++ )
			{
				final double rGanados = Math.round( Math.pow( pGanados, i )*10000 )/100;
				final double rPerdidos = Math.round( Math.pow( pPerdidos, i )*10000 ) /100;
				final double rEmpatados = Math.round( Math.pow( pEmpatados, i )*10000 ) /100;
				final double rGanaEmpata = Math.round( ( rGanados +rEmpatados ) );
				final double rPierdeEmpata =Math.round(  ( rPerdidos +rEmpatados ) );
				final double rGanaPierde = Math.round( ( rGanados +rPerdidos ) );
				msg += rGanados+"% \t "+rPerdidos+"% \t"+rEmpatados+"% \t "+rGanaEmpata+"% \t "+rPierdeEmpata+"% \t "+rGanaPierde+"%\n";
			}
			return msg;
		}
		throw new Exception( "El equipo no cuenta con los partidos suficientes para inferir en Paroli\n"+"Numero de partidos:"+totalPartidos );
	}

	public void inOrder()
	{
		if( izquierda != null )
		{
			izquierda.inOrder();
		}
		//JOptionPane.showMessageDialog( null, darLogo() );
		try
		{
			System.out.println( darNombre()+"-"+primero.calcularPuntos( darNombre() )+"\n"+darResumenEstrategiaParoli() );
		}
		catch (Exception e)
		{
			System.err.println( e.getMessage() );
		}
		if( derecha != null )
		{
			derecha.inOrder();
		}
	}

	public String buscarEncuentro( final String pEquipo ) throws Exception
	{
		if( primero == null )
		{
			throw new Exception( "No hay partidos para este equipo aun" );
		}
		return primero.buscarEncuentro( pEquipo,darNombre() );
	}

	@Override
	public String toString()
	{
		return "Equipo\n" +
				"{" +
					"nombre=" + nombre+","+
					"No Partidos="+contarPartidos()+
				'}';
	}
}