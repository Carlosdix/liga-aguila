package Mundo;

import BaseDeDatos.ConexionBaseDeDatos;
import BaseDeDatos.EquipoBD;
import BaseDeDatos.PartidoBD;
import BaseDeDatos.ResumenBD;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Liga_Aguila
{
    private Equipo raiz;

    private final ConexionBaseDeDatos conexionBaseDeDatos;

    private EquipoBD equipoBD;

    private PartidoBD partidoBD;

    private ResumenBD resumenBD;

    public Liga_Aguila()  throws ClassNotFoundException, SQLException, Exception
    {
        raiz = null;
        conexionBaseDeDatos = new ConexionBaseDeDatos();
        equipoBD = new EquipoBD( conexionBaseDeDatos );
        partidoBD = new PartidoBD( conexionBaseDeDatos );
        resumenBD = new ResumenBD( conexionBaseDeDatos );
    }

    public Equipo buscarEquipo( final String pNombre )throws Exception
    {
        if( raiz != null )
        {

            return raiz.buscarEquipo( pNombre );
        }
        throw new Exception( "No hay equipos" );
    }

    public void agregarEquipo(final String pNombre, final String pRuta ) throws Exception
    {
        equipoBD.agregarEquipo( pNombre,pRuta );
    }

    public void agregarPartido( final String pNombreLocal, final String pNombreVisitante, final  int pGolesLocal,
                                final int pGolesvisitante, final int pTALocal, final int pTAVisitante, final int pTRLocal,
                                final int pTRVisitante ) throws SQLException, FileNotFoundException, Exception
    {
        partidoBD.agregarPartido( pNombreLocal, pNombreVisitante, pGolesLocal, pGolesvisitante, pTALocal, pTAVisitante, pTRLocal, pTRVisitante );
    }

    private void cargarInformacion() throws SQLException, IOException, Exception
    {
        for( int i = 0 ; i < 20 ; i++ )
        {
            FileChooser
        }

        final ResultSet resultSet = equipoBD.darEquipos();
        while ( resultSet.next() )
        {
            final String nombre = resultSet.getString( 1 );
            final ImageIcon imageIcon = new ImageIcon( ImageIO.read( resultSet.getBinaryStream(2 ) ) );
            if (raiz != null)
            {
                raiz.agregarEquipo( nombre, imageIcon);
            }
            else
            {
                raiz = new Equipo( nombre, imageIcon);
            }
            cargarPartidosEquipo( nombre );
        }
    }

    private void cargarPartidosEquipo( final String pNombre )throws SQLException,Exception
    {
        final ResultSet resultSet = partidoBD.darPartidosEquipo( pNombre );
        final Equipo equipo = buscarEquipo( pNombre );
        while ( resultSet.next() )
        {
            final String local = resultSet.getString( 1 );
            final String visitante = resultSet.getString( 2 );
            final int goles_l = resultSet.getInt( 3 );
            final int goles_v = resultSet.getInt( 4 );
            final int tAL = resultSet.getInt( 5 );
            final int tAV = resultSet.getInt( 6 );
            final int tRL = resultSet.getInt( 7 );
            final int tRV = resultSet.getInt( 8 );
            equipo.agregarPartido( local,visitante,goles_l,goles_v,tAL,tAV,tRL,tRV );
        }
    }

    public static void main( final String args[] )
    {
        try
        {
            final Liga_Aguila liga_aguila = new Liga_Aguila();
            liga_aguila.cargarInformacion();
            liga_aguila.raiz.inOrder();
        }
        catch (Exception e)
        {
            System.err.println( e.getMessage() );
            e.printStackTrace();
        }
    }
}