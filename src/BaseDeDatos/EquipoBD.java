package BaseDeDatos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EquipoBD
{
    private final ConexionBaseDeDatos conexionBaseDeDatos;

    public EquipoBD(final ConexionBaseDeDatos pConexionBaseDeDatos ) throws ClassNotFoundException, SQLException, Exception
    {
        conexionBaseDeDatos = pConexionBaseDeDatos;
    }

    public void agregarEquipo(final String pNombre, final String pRuta ) throws SQLException, FileNotFoundException, Exception
    {
        final File file = new File( pRuta );
        if ( !file.exists() )
        {
            throw new Exception( "El archivo del logo del equipo no existe!" );
        }
        final PreparedStatement statement = conexionBaseDeDatos.darPreparedStatement( "INSERT INTO equipo VALUES(?,?);" );
        statement.setString( 1, pNombre );
        statement.setBinaryStream( 2, new FileInputStream( pRuta ), (int) file.length() );
        statement.executeUpdate();
        statement.close();
    }

    public ResultSet darEquipos() throws SQLException
    {
        return conexionBaseDeDatos.darResultSet( "SELECT * FROM equipo;" );
    }
}

