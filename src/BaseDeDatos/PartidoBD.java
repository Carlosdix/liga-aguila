package BaseDeDatos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PartidoBD
{
    private final ConexionBaseDeDatos conexionBaseDeDatos;

    public PartidoBD(final ConexionBaseDeDatos pConexionBaseDeDatos ) throws ClassNotFoundException, SQLException, Exception
    {
        conexionBaseDeDatos = pConexionBaseDeDatos;
    }

    public void agregarPartido( final String pNombreLocal, final String pNombreVisitante, final  int pGolesLocal,
                                final int pGolesvisitante, final int pTALocal, final int pTAVisitante, final int pTRLocal,
                                final int pTRVisitante ) throws SQLException, FileNotFoundException, Exception
    {
        final PreparedStatement statement = conexionBaseDeDatos.darPreparedStatement( "INSERT INTO partido VALUES(?,?,?,?,?,?,?,?);" );
        statement.setString( 1, pNombreLocal );
        statement.setString( 2, pNombreVisitante );
        statement.setInt( 3, pGolesLocal );
        statement.setInt( 4, pGolesvisitante );
        statement.setInt( 5, pTALocal );
        statement.setInt( 6, pTAVisitante );
        statement.setInt( 7, pTRLocal );
        statement.setInt( 8, pTRVisitante );
        statement.executeUpdate();
        statement.close();
    }

    public ResultSet darPartidosEquipo( final String pNomrbeEquipo ) throws SQLException
    {
        return conexionBaseDeDatos.darResultSet( "SELECT * FROM partido WHERE equipo_l = '"+pNomrbeEquipo+"' OR equipo_v = '"+pNomrbeEquipo+"';" );
    }
}
