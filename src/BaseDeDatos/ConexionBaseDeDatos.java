package BaseDeDatos;

import java.sql.*;

public class ConexionBaseDeDatos
{
    //---------------------------------------------Constants---------------------------------------------

    /**
     * Direccion IP del servidor de base de datos
     */
    private final static String DIRECCION = "localhost";

    /**
     * Puerto de conexion a la base de datos
     */
    private final static String PUERTO = "5432";

    /**
     * Nombre de la base de datos
     */
    private final static String NOMBRE = "Liga Aguila";

    /**
     * URL de conexion para la .
     */
    private final static String URL = "jdbc:postgresql://"+ DIRECCION +":"+ PUERTO +"/"+ NOMBRE;

    /**
     * Usuario de base de datos
     */
    private final static String USER = "postgres";

    /**
     * Contraseña de base de datos
     */
    private final static String PASS = "7423102Ca";

    //---------------------------------------------Attributes---------------------------------------------

    /**
     * Variable de conexion a la base de datos
     */
    private final Connection conexion;

    //---------------------------------------------Constructor---------------------------------------------

    /**
     * Se encarga de inicilizar la variable de conexion y conectar a la base de datos
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public ConexionBaseDeDatos() throws ClassNotFoundException, SQLException, Exception
    {
        Class.forName("org.postgresql.Driver");
        conexion = DriverManager.getConnection(URL, USER, PASS);
        if( conexion == null )
        {
            throw new Exception("No se pudo encontrar la base de datos");
        }
    }

    public PreparedStatement darPreparedStatement(final String pSQL ) throws SQLException
    {
        return conexion.prepareStatement( pSQL );
    }

    public ResultSet darResultSet(final String pSQL ) throws SQLException
    {
        return conexion.createStatement().executeQuery( pSQL );
    }
}
