package BaseDeDatos;

import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ResumenBD
{
    private final ConexionBaseDeDatos conexionBaseDeDatos;

    public ResumenBD(final ConexionBaseDeDatos pConexionBaseDeDatos ) throws ClassNotFoundException, SQLException, Exception
    {
        conexionBaseDeDatos = pConexionBaseDeDatos;
    }

    public void agregarResumen( final String pNombre ) throws SQLException, FileNotFoundException, Exception
    {
        final PreparedStatement statement = conexionBaseDeDatos.darPreparedStatement( "INSERT INTO resumen (equipo) VALUES(?);" );
        statement.setString( 1, pNombre );
        statement.executeUpdate();
        statement.close();
    }

    public ResultSet darResumen( final String pNomrbeEquipo ) throws SQLException
    {
        return conexionBaseDeDatos.darResultSet( "SELECT * FROM resumen WHERE equipo='"+pNomrbeEquipo+"';" );
    }
}
