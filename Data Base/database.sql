CREATE TABLE equipo
(
    nombre varchar( 35 ) NOT NULL PRIMARY KEY,
    logo bytea NOT NULL
);

CREATE TABLE partido
(
    equipo_l varchar( 35 ) NOT NULL,
    equipo_v varchar( 35 ) NOT NULL,
    goles_l SMALLINT NOT NULL,
    goles_v SMALLINT NOT NULL,
    tarjetas_amarillas_l SMALLINT NOT NULL,
    tarjetas_amarillas_v SMALLINT NOT NULL,
    tarjetas_rojas_l SMALLINT NOT NULL,
    tarjetas_rojas_v SMALLINT NOT NULL,
    FOREIGN KEY (equipo_l) REFERENCES equipo( nombre ),
    FOREIGN KEY (equipo_v) REFERENCES equipo( nombre )
);

CREATE TABLE evento
(
    equipo VARCHAR(35) NOT NULL,
    puntos SMALLINT NOT NULL DEFAULT 0,
    diferencia_goles SMALLINT NOT NULL DEFAULT 0,
    amarillas SMALLINT NOT NULL DEFAULT 0,
    rojas SMALLINT NOT NULL DEFAULT 0
);

CREATE TABLE resumen
(
    equipo varchar(35) NOT NULL,
    ganados SMALLINT NOT NULL DEFAULT 0,
    perdidos SMALLINT NOT NULL DEFAULT 0,
    empatados SMALLINT NOT NULL DEFAULT 0,
    puntuacion DOUBLE PRECISION NOT NULL DEFAULT 0,
    diferencia_goles DOUBLE PRECISION NOT NULL DEFAULT 0,
    goles_favor DOUBLE PRECISION NOT NULL DEFAULT 0,
    goles_contra DOUBLE PRECISION NOT NULL DEFAULT 0,
    amarillas SMALLINT NOT NULL DEFAULT 0,
    rojas SMALLINT NOT NULL DEFAULT 0,
    CONSTRAINT resumen_equipo_fkey FOREIGN KEY (equipo) REFERENCES equipo (nombre)
);

--//-----------------------------------------------------------------------------------------------------------//-------

CREATE OR REPLACE FUNCTION completar_evento()
    RETURNS TRIGGER AS
$$
DECLARE
    miPartido partido := new;
BEGIN
    IF ( miPartido.goles_l = miPartido.goles_v )THEN
        INSERT INTO evento VALUES ( miPartido.equipo_l, 1,0, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l );
        INSERT INTO evento VALUES ( miPartido.equipo_v, 1,0, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v );
    ELSE
        IF ( miPartido.goles_l > miPartido.goles_v )THEN
            INSERT INTO evento VALUES
            (
                miPartido.equipo_l, 3,miPartido.goles_l-miPartido.goles_v, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l
            );
            INSERT INTO evento VALUES
            (
                miPartido.equipo_v, 0,miPartido.goles_v-miPartido.goles_l, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v
            );
        ELSE
            INSERT INTO evento VALUES
            (
                miPartido.equipo_l, 0,miPartido.goles_l-miPartido.goles_v, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l
            );
            INSERT INTO evento VALUES
            (
                miPartido.equipo_v, 3,miPartido.goles_v-miPartido.goles_l, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v
            );
        END IF;
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_evento AFTER INSERT ON partido
    FOR EACH ROW
EXECUTE PROCEDURE completar_evento();
CREATE OR REPLACE FUNCTION testAgregarEvento() RETURNS VOID
AS
$$
DECLARE
    miPartido partido;
BEGIN
    FOR miPartido IN SELECT * FROM partido
        LOOP
            IF ( miPartido.goles_l = miPartido.goles_v )THEN
                INSERT INTO evento VALUES ( miPartido.equipo_l, 1,0, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l );
                INSERT INTO evento VALUES ( miPartido.equipo_v, 1,0, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v );
            ELSE
                IF ( miPartido.goles_l > miPartido.goles_v )THEN
                    INSERT INTO evento VALUES
                    (
                        miPartido.equipo_l, 3,miPartido.goles_l-miPartido.goles_v, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l
                    );
                    INSERT INTO evento VALUES
                    (
                        miPartido.equipo_v, 0,miPartido.goles_v-miPartido.goles_l, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v
                    );
                ELSE
                    INSERT INTO evento VALUES
                    (
                        miPartido.equipo_l, 0,miPartido.goles_l-miPartido.goles_v, miPartido.tarjetas_amarillas_l, miPartido.tarjetas_rojas_l
                    );
                    INSERT INTO evento VALUES
                    (
                        miPartido.equipo_v, 3,miPartido.goles_v-miPartido.goles_l, miPartido.tarjetas_amarillas_v, miPartido.tarjetas_rojas_v
                    );
                END IF;
            END IF;
        END LOOP;
END;
$$
LANGUAGE plpgsql;
SELECT testAgregarEvento();

CREATE OR REPLACE FUNCTION testActualizarResumen()
RETURNS VOID AS
    $$
    DECLARE
        miEvento evento;
    BEGIN
        FOR miEvento IN SELECT * FROM evento
        LOOP
            IF ( miEvento.puntos = 3 ) THEN
                UPDATE resumen SET
                                   ganados=ganados+1,
                                   puntuacion=puntuacion+miEvento.puntos,
                                   diferencia_goles=diferencia_goles+miEvento.diferencia_goles,
                                   amarillas = amarillas + miEvento.amarillas,
                                   rojas = rojas + miEvento.rojas
                WHERE resumen.equipo=miEvento.equipo;
            ELSE
                IF ( miEvento.puntos = 1 ) THEN
                    UPDATE resumen SET
                                       empatados=empatados+1,
                                       puntuacion=puntuacion+miEvento.puntos,
                                       diferencia_goles=diferencia_goles+miEvento.diferencia_goles,
                                       amarillas = amarillas + miEvento.amarillas,
                                       rojas = rojas + miEvento.rojas
                    WHERE resumen.equipo=miEvento.equipo;
                ELSE
                    UPDATE resumen SET
                                       perdidos=perdidos+1,
                                       puntuacion=puntuacion+miEvento.puntos,
                                       diferencia_goles=diferencia_goles+miEvento.diferencia_goles,
                                       amarillas = amarillas + miEvento.amarillas,
                                       rojas = rojas + miEvento.rojas
                    WHERE resumen.equipo=miEvento.equipo;
                END IF;
            END IF;
        END LOOP;
    END;
    $$
LANGUAGE plpgsql;

SELECT testAgregarEvento();
SELECT testActualizarResumen();

CREATE TRIGGER actualizar_resumen_a_u AFTER INSERT ON evento
    FOR EACH ROW
EXECUTE PROCEDURE actualizar_tabla();

TRUNCATE TABLE evento;
TRUNCATE TABLE resumen;

SELECT * FROM equipo;
SELECT * FROM partido;
SELECT * FROM evento;
SELECT * FROM evento WHERE equipo='La Equidad';
SELECT * FROM resumen;

